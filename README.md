# Common

Common functions and datatypes

**Datatypes**
* `IcaoCode`

## Installation
Install with NuGet:

```
dotnet add package PilotAppLibs.Common
```

## License

This repository is licensed with the [MIT](LICENSE) license

## Author

Simon Mårtensson (martensi)
