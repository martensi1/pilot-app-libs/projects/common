using System;
using Xunit;

namespace PilotAppLib.Common.Tests
{
    public class IcaoCodeTests
    {
        [Fact]
        public void Constructor()
        {
            // Arrange
            string value = "ESSA";

            // Act
            var code = new IcaoCode(value);
            
            // Assert
            Assert.Equal(value, code.ToString());
        }
        
        [Fact]
        public void ImplicitConstructor()
        {
            // Arrange
            string value = "EHAM";

            // Act
            IcaoCode code = value;

            // Assert
            Assert.Equal(value, code.ToString());
        }

        [Fact]
        public void ImplicitConstructorNull()
        {
            // Arrange
            string value = null;

            // Act
            IcaoCode code = value;

            // Assert
            Assert.True(object.ReferenceEquals(value, code));
        }

        [Fact]
        public void ImplicitStringConstructor()
        {
            // Arrange
            var code = new IcaoCode("EHAM");
            
            // Act
            string value = code;

            // Assert
            Assert.Equal(code.ToString(), value);
        }

        [Fact]
        public void ImplicitStringConstructorNull()
        {
            // Arrange
            IcaoCode code = null;

            // Act
            string value = code;

            // Assert
            Assert.True(object.ReferenceEquals(code, value));
        }

        [Theory]
        [InlineData("ESSA ")]
        [InlineData(" KLAX")]
        [InlineData("EHAM1")]
        [InlineData("2EGLL")]
        [InlineData("ESGJJ")]
        [InlineData("lfpg")]
        [InlineData("EsGg")]
        public void Constructor_InvalidCode(string invalidInput)
        {
            // Act and assert
            var ex = Assert.Throws<ArgumentException>(() =>
            {
                new IcaoCode(invalidInput);
            });

            Assert.Equal("Invalid ICAO code", ex.Message);
        }

        [Fact]
        public void Constructor_NullArgument()
        {
            // Act and assert
            var ex = Assert.Throws<ArgumentNullException>(() =>
            {
                new IcaoCode(null);
            });

            Assert.Equal("code", ex.ParamName);
        }

        [Theory]
        [InlineData("LPFR", true)]
        [InlineData("ESSA ", false)]
        [InlineData(" KLAX", false)]
        [InlineData("EHAM1", false)]
        [InlineData("2EGLL", false)]
        [InlineData("ESGJJ", false)]
        [InlineData("lfpg", false)]
        [InlineData("EsGg", false)]
        public void IsValidCode(string inputCode, bool expectedResult)
        {
            // Act and assert
            Assert.Equal(expectedResult, IcaoCode.IsValidCode(inputCode));
        }

        [Fact]
        public void IsValidCode_NullArgument()
        {
            // Act and assert
            var ex = Assert.Throws<ArgumentNullException>(() =>
            {
                IcaoCode.IsValidCode(null);
            });

            Assert.Equal("code", ex.ParamName);
        }

        [Theory]
        [InlineData("ESGJ", "ESGJ", true)]
        [InlineData("ESGJ", "EHAM", false)]
        [InlineData("LPFR", "LPFR", true)]
        [InlineData("EGLL", "EHAM", false)]
        public void EqualsAndHashcode(string inputCode1, string inputCode2, bool expectedValue)
        {
            // Arrange
            var icaoCode1 = new IcaoCode(inputCode1);
            var icaoCode2 = new IcaoCode(inputCode2);

            // Act and assert
            Assert.Equal(expectedValue, icaoCode1.Equals(icaoCode2));
            Assert.Equal(expectedValue, icaoCode1.GetHashCode() == icaoCode2.GetHashCode());
            
            Assert.Equal(expectedValue, icaoCode1 == icaoCode2);
            Assert.Equal(!expectedValue, icaoCode1 != icaoCode2);
        }

        [Fact]
        public void EqualsObject()
        {
            // Arrange
            IcaoCode icaoCode = new IcaoCode("EHAM");

            object icaoCodeEqual = new IcaoCode("EHAM");
            object icaoCodeNotEqual = new IcaoCode("EGLL");
            object nullValue = null;
            object stringValue = "This is a string";

            // Act and assert
            Assert.True(icaoCode.Equals(icaoCodeEqual));
            Assert.False(icaoCode.Equals(icaoCodeNotEqual));
            Assert.False(icaoCode.Equals(nullValue));
            Assert.False(icaoCode.Equals(stringValue));
        }

        [Fact]
        public void EqualsNull()
        {
            // Arrange
            var icaoCode = new IcaoCode("EHAM");

            // Act and assert
            Assert.False(icaoCode.Equals((IcaoCode)null));
            Assert.False(icaoCode.Equals((object)null));
            
            Assert.False(icaoCode == null);
            Assert.False(null == icaoCode);

            Assert.True(icaoCode != null);
            Assert.True(null != icaoCode);

            Assert.True((IcaoCode)null == (IcaoCode)null);
        }

        [Fact]
        public void CompareTo()
        {
            // Arrange
            var icaoCode1 = new IcaoCode("EHAM");
            var icaoCode2 = new IcaoCode("EGLL");
            var icaoCode3 = new IcaoCode("EHAM");

            // Act and assert
            Assert.True(icaoCode1.CompareTo(icaoCode2) > 0);
            Assert.True(icaoCode2.CompareTo(icaoCode1) < 0);
            Assert.True(icaoCode1.CompareTo(icaoCode3) == 0);
        }
    }
}
