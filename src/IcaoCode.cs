﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace PilotAppLib.Common
{
    /// <summary>
    /// ICAO code object, respresents a ICAO airport code
    /// </summary>
    [DebuggerDisplay("IcaoCode={_code}")]

    public sealed class IcaoCode :
        IEquatable<IcaoCode>,
        IComparable<IcaoCode>
    {
        private readonly string _code;


        /// <summary>Initializes a new instance of the <see cref="IcaoCode" /> class</summary>
        /// <exception cref="ArgumentNullException">Input code argument is a null value</exception>
        /// <exception cref="ArgumentException">Invalid ICAO code</exception>

        public IcaoCode(string code)
        {
            CheckIfValidCodeOrThrow(code);
            _code = code;
        }

        /// <summary>Creates a <see cref="IcaoCode" /> object from a string value</summary>
        /// <exception cref="ArgumentNullException">Input code argument is a null value</exception>
        /// <exception cref="ArgumentException">Invalid ICAO code</exception>
        /// <returns>A <see cref="IcaoCode" /> object</returns>
        public static implicit operator IcaoCode(string code)
        {
            if (code == null)
                return null;

            return new IcaoCode(code);
        }

        /// <summary>Creates a string value from a <see cref="IcaoCode" /> object</summary>
        /// <returns>A icao code as string</returns>
        public static implicit operator string(IcaoCode code)
        {
            if (code == null)
                return null;

            return code.ToString();
        }


        /// <summary>Determines whether the specified object is equal to the current object</summary>
        /// <returns><see langword="true"/> if the specified object is equal to the current object; otherwise, <see langword="false"/></returns>
        public override bool Equals(object other)
        {
            return other is IcaoCode value &&
                this.Equals(value);
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type</summary>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/></returns>
        public bool Equals(IcaoCode other)
        {
            if (object.ReferenceEquals(other, null))
                return false;

            return this._code == other._code;
        }

        /// <summary>Indicates whether the current object is equal to another object</summary>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/></returns>
        public static bool operator ==(IcaoCode left, IcaoCode right)
        {
            if (object.ReferenceEquals(left, null) && object.ReferenceEquals(right, null))
                return true;

            if (object.ReferenceEquals(left, null) || object.ReferenceEquals(right, null))
                return false;

            return left.Equals(right);
        }

        /// <summary>Indicates whether the current object is not equal to another object</summary>
        /// <returns><see langword="true"/> if the current object is not equal to the other parameter; otherwise, <see langword="false"/></returns>
        public static bool operator !=(IcaoCode left, IcaoCode right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance
        /// precedes, follows, or occurs in the same position in the sort order as the other object
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has these meanings:
        /// Less than zero: This instance precedes other in the sort order
        /// Zero: This instance occurs in the same position in the sort order as other
        /// Greater than zero: This instance follows the other in the sort order
        /// </returns>
        public int CompareTo(IcaoCode other)
        {
            return _code.CompareTo(other._code);
        }

        /// <summary>Serves as the default hash function</summary>
        /// <returns>A hash code for the current object</returns>
        public override int GetHashCode()
        {
            return _code.GetHashCode();
        }

        /// <summary>Returns a string that represents the current object</summary>
        /// <returns>A string that represents the current object</returns>
        public override string ToString()
        {
            return _code;
        }


        /// <summary>Determines whether a string is a valid ICAO code</summary>
        /// <returns><see langword="true"/> if the specified string is a valid ICAO code; otherwise, <see langword="false"/></returns>
        public static bool IsValidCode(string code)
        {
            if (code == null)
            {
                throw new ArgumentNullException(nameof(code));
            }

            return Regex.IsMatch(code, "^[A-Z]{4}$");
        }

        private static void CheckIfValidCodeOrThrow(string input)
        {
            if (!IsValidCode(input))
                throw new ArgumentException("Invalid ICAO code");
        }
    }
}
